input {
    tcp {
        type => syslog
        port => 5014
    }
    udp {
        type => syslog
        port => 5014
    }
 
}

# This is the logstash-filter to process logfiles from a pfsense Firewall
filter {
    # Replace with your IP
    if [host] =~ PFSENSE_SRV {
        grok {
           match => [ 'message', '.* %{WORD:program}:%{GREEDYDATA:rest}' ]
        }

        if [program] == "filterlog" {
            # Grab fields up to IP version. The rest will vary depending on IP version.
            grok {  
                match => [ 'rest', '%{INT:rule_number},%{INT:sub_rule_number},,%{INT:tracker_id},%{WORD:interface},%{WORD:reason},%{WORD:action},%{WORD:direction},%{WORD:ip_version},%{GREEDYDATA:rest2}' ] 
            }
        }

        mutate {
          replace => [ 'message', '%{rest2}' ]
        }

        if [ip_version] == "4" {
            # IPv4. Grab field up to dest_ip. Rest can vary.
            grok {
                match => [ 'message', '%{WORD:tos},,%{INT:ttl},%{INT:id},%{INT:offset},%{WORD:flags},%{INT:protocol_id},%{WORD:protocol},%{INT:length},%{IP:src_ip},%{IP:dest_ip},%{GREEDYDATA:rest3}' ]
            }
        }

        if [protocol_id] != 2 {
            # Non-IGMP has more fields.
            grok {
                match => [ 'rest3', '^%{INT:src_port:int},%{INT:dest_port:int}' ]
            }
        }

        else {
            # IPv6. Grab field up to dest_ip. Rest can vary.
            grok {
                match => [ 'message', '%{WORD:class},%{WORD:flow_label},%{INT:hop_limit},%{WORD:protocol},%{INT:protocol_id},%{INT:length},%{IPV6:src_ip},%{IPV6:dest_ip},%{GREEDYDATA:rest3}' ]        
            }
        }

        mutate {
            replace => [ 'message', '%{rest3}' ]
            lowercase => [ 'protocol' ]
        }

        if [message] {
            # Non-ICMP has more fields
            grok {
                match => [ 'message', '^%{INT:src_port:int},%{INT:dest_port:int},%{INT:data_length}' ]
            }
        }

        mutate {
            remove_field => [ 'message' ]
            remove_field => [ 'rest' ]
            remove_field => [ 'rest2' ]
            remove_field => [ 'rest3' ]
            remove_tag => [ '_grokparsefailure' ]
            add_tag => [ 'packetfilter' ]
        }

        geoip {
            add_tag => [ "GeoIP" ]
            source => "src_ip"
        }
    }
}

output {
  elasticsearch {
    host => ["ES_CONN_STR"]
    index => "logstash-%{+YYYY.MM.dd}"
  }
stdout { codec => rubydebug }
}
